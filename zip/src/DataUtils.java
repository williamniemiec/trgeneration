public class DataUtils {
	
	public static String generateDirectoryPathFromSignature(String invokedSignature, 
			boolean isConstructor) {
		String[] signatureFields = extractSignatureFields(invokedSignature);
		
		String folderPath = getFolderPath(signatureFields, isConstructor);
		String folderName = getFolderName(signatureFields, isConstructor) 
				+ reduceSignature(extractParametersFromSignature(invokedSignature));
		
		return folderPath + "/" + replaceReservedCharacters(folderName);
	}
	
	private static String[] extractSignatureFields(String signature) {
		String signatureWithoutParameters = signature.contains("(")
				? signature.substring(0, signature.indexOf("("))
				: signature;
				
		return	signatureWithoutParameters.split("\\.");
	}	
	
	private static String reduceSignature(String parameters) {
		return parameters.replaceAll("([^.(\\s]+\\.)+", "");
	}
	
	private static String extractParametersFromSignature(String signature) {
		if (!signature.contains("("))
			return "()";
		
		return signature.substring(signature.indexOf("("));
	}
	
	private static String getFolderPath(String[] signatureFields, boolean isConstructor) {
		StringBuilder folderPath = new StringBuilder();
		int size = isConstructor ? signatureFields.length-1 : signatureFields.length-2;
		
		for (int i=0; i<size; i++) {
			folderPath.append(signatureFields[i]);
			folderPath.append("/");
		}
		
		// Removes last slash
		if (folderPath.length() > 0) {
			folderPath.deleteCharAt(folderPath.length()-1);	
		}
		
		return folderPath.toString();
	}
	
	private static String getFolderName(String[] signatureFields, boolean isConstructor) {
		String folderName = "";
		
		if (isConstructor) {
			String className = signatureFields[signatureFields.length-1];
			
			folderName = className;
		}
		else {			
			String className = signatureFields[signatureFields.length-2];
			String methodName = signatureFields[signatureFields.length-1];
			
			folderName = className + "." + methodName;
		}
		
		return replaceReservedCharacters(folderName);
	}
	
	public static String replaceReservedCharacters(String str) {
		String processedStr = str;
		
		processedStr = processedStr.replaceAll("\\<", "(");
		processedStr = processedStr.replaceAll("\\>", ")");
		processedStr = processedStr.replaceAll("\\/", "-");
		processedStr = processedStr.replaceAll("\\\\", "-");
		processedStr = processedStr.replaceAll("\\:", ";");
		processedStr = processedStr.replaceAll("\"", "'");
		processedStr = processedStr.replaceAll("\\|", "-");
		processedStr = processedStr.replaceAll("\\?", "+");
		processedStr = processedStr.replaceAll("\\*", "+");
		
		return processedStr;
	}
	
	public static String methodSignatureWithoutParameterNames(String methodSignature) {
		String processedMethodSignature = methodSignature;
		
		processedMethodSignature = processedMethodSignature.replaceAll("final ", "");
		
		String onlyParameters = processedMethodSignature.substring(processedMethodSignature.indexOf("(")+1, processedMethodSignature.lastIndexOf(")"));
		String beforeParameters = processedMethodSignature.substring(0, processedMethodSignature.indexOf("("));
		
		return beforeParameters + "(" + removeParameterVariables(onlyParameters) + ")";
	}
	
	private static String removeParameterVariables(String parameters) {
		StringBuilder parametersWithoutVariables = new StringBuilder();
		
		for (String term : parameters.split(",")) {
			parametersWithoutVariables.append(term.trim().split(" ")[0]);
			parametersWithoutVariables.append(", ");
		}
		
		// Removes last comma
		if (parametersWithoutVariables.length() > 0) {
			parametersWithoutVariables.deleteCharAt(parametersWithoutVariables.length() - 1);
			parametersWithoutVariables.deleteCharAt(parametersWithoutVariables.length() - 1);
		}
		
		return parametersWithoutVariables.toString();
	}
}
