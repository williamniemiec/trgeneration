

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Set;

/**
 * Responsible for searching files.
 * 
 * @author		William Niemiec &lt; williamniemiec@hotmail.com &gt;
 */
public class FileSearcher {
	
	//-------------------------------------------------------------------------
	//		Attributes
	//-------------------------------------------------------------------------
	private final Path workingDirectory; 
	
	
	//-------------------------------------------------------------------------
	//		Constructors
	//-------------------------------------------------------------------------
	public FileSearcher(Path workingDirectory) {
		if (workingDirectory == null)
			this.workingDirectory = Path.of(".");
		else
			this.workingDirectory = workingDirectory;
	}
	
	public FileSearcher() {
		this(null);
	}
	
	
	//-------------------------------------------------------------------------
	//		Methods
	//-------------------------------------------------------------------------
	public Set<Path> findAllFilesWithExtension(String extension) throws IOException {
		Set<Path> paths = new HashSet<>();
		
		Files.walkFileTree(workingDirectory, new SimpleFileVisitor<Path>() {
			@Override
		    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
		        if (file.toString().endsWith("." + extension)) {
		        	paths.add(file.toAbsolutePath().normalize());
		        }
		        
		        return FileVisitResult.CONTINUE;
			}
		});
		
		return paths;
	}
}
