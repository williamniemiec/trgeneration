import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class TRGeneration {
	private static CodeProcessor processor;
	private static List<String> filesToProcess;

	public static void main(String[] args) throws IOException {
		filesToProcess = new ArrayList<String>();

		if (args.length < 1) {
			System.err.println("You must supply an input file or folder");
			System.exit(1);
		}

		Options options = new Options();
		options.addOption("d", false, "Print debug output"); // does not have a value
		options.addOption("o", true, "PNG output path"); // does not have a value
		options.addOption("g", false, "Print graph structures");
		options.addOption("l", false, "Print line flows");
		options.addOption("t", false, "Print PPC and EC test requirements");
		options.addOption("T", false, "Print complete test requirements");

		CommandLineParser parser = new BasicParser();
		CommandLine cmd = null;
		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.err.println("Caught ParseException: " + e.getMessage());
		}
				
		//String inputPath = args[args.length - 1];
		Path srcRootDirectory = Path.of(args[args.length - 1]);
		Set<Path> paths = findAllSourceFiles(srcRootDirectory);
		
		for (Path srcFile : paths) {
			try {
				System.out.println("Analyzing " + srcFile);
				parseFile(cmd, srcFile.toFile(), srcRootDirectory);
				System.out.println("Ok\n");
			} 
			catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}

	private static Set<Path> findAllSourceFiles(Path projectSourcePath) throws IOException {
		FileSearcher searcher = new FileSearcher(projectSourcePath);
		
		return searcher.findAllFilesWithExtension("java");
	}
	
	private static void parseFile(CommandLine cmd, File inputFile, Path workingDirectory) 
			throws Exception {
		boolean isInputDirectory = inputFile.isDirectory();

		if (isInputDirectory) {
			readDirectory(inputFile);
		} else {
			filesToProcess.add(inputFile.toString());
		}
		
		for (String filePath : filesToProcess) {
			processor = new CodeProcessor(filePath, workingDirectory);
			readSource(filePath);
			
			try {
				if (cmd.hasOption("d")) processor.setDebug(true);
				processor.build();
				
				if (cmd.hasOption("g")) processor.writeGraphStructures();
				if (cmd.hasOption("l")) processor.writeLineEdges();
				if (cmd.hasOption("T")) processor.writeTestRequirements();
				if (cmd.hasOption("t")) processor.writePPCandECrequirements();
			} catch(Exception e) {
				System.err.println("Error while processing file " + filePath + ":");
				throw e;
			}
			processor.clear();
		}
	}

	private static void readSource(String path) throws Exception {
		FileInputStream fstream = null;

		try {
			fstream = new FileInputStream(path);
		} catch (IOException e) {
			System.err.println("Unable opening file " + path + ".\n" + e.getMessage());
//			System.exit(1);
			throw new Exception();
		}

		Scanner s = new Scanner(fstream);
		while (s.hasNextLine()) {
			processor.addSourceCodeLine(s.nextLine());
		}
		s.close();
		try {
			fstream.close();
		} catch (IOException e) {
			System.err.println("Error closing file " + path + ".\n" + e.getMessage());
		}
	}

	private static void readDirectory(File dir) {
		for(File f : dir.listFiles()) {
			if (f.isDirectory()) {
				readDirectory(f);
			}
			else if (f.getPath().substring(f.getPath().lastIndexOf('.')+1).equals("java")) {
				filesToProcess.add(f.getPath());
			}
		}
	}
}
